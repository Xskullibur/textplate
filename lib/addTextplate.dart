import 'package:flutter/material.dart';

class ConfigureTextplatePage extends StatefulWidget {
  const ConfigureTextplatePage({Key? key, required this.name})
      : super(key: key);
  final String name;

  @override
  _ConfigureTextplatePageState createState() => _ConfigureTextplatePageState();
}

class _ConfigureTextplatePageState extends State<ConfigureTextplatePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.name),
      ),
      body: ReorderableListView(
        children: const [
          ListTile(
            key: Key("TextplateTitleTile"),
            contentPadding: EdgeInsets.all(16.0),
            title: TextField(
              key: Key("TextplateTitleField"),
              decoration: InputDecoration(
                  border: OutlineInputBorder(), labelText: 'Title'),
            ),
          )
        ],
        onReorder: (oldIndex, newIndex) {},
      ),
    );
  }
}
